package sms

import (
	"gitee.com/sansaniot/ssiot-core/msg/sms/sdk/ali"
)

func GetTemplateList(list *[]ali.Template) error {
	aliSdk := ali.TemplateSdk{Config: smsConfig}
	return aliSdk.GetTemplateList(list)
}

func AddTemplate(addTemplate *ali.Template) error {
	aliSdk := ali.TemplateSdk{Config: smsConfig}
	return aliSdk.AddTemplate(addTemplate)
}

func DeleteTemplate(templateCode string) error {
	aliSdk := ali.TemplateSdk{Config: smsConfig}
	return aliSdk.DeleteTemplate(templateCode)
}
