package ali

import (
	"errors"
	"gitee.com/sansaniot/ssiot-core/msg/sms/model"
	dysmsapi "github.com/alibabacloud-go/dysmsapi-20170525/v2/client"
)

type Template struct {
	AuditStatus     *string                                                         `json:"auditStatus,omitempty" xml:"AuditStatus,omitempty"`
	CreateDate      *string                                                         `json:"createDate,omitempty" xml:"CreateDate,omitempty"`
	OrderId         *string                                                         `json:"orderId,omitempty" xml:"OrderId,omitempty"`
	Reason          *dysmsapi.QuerySmsTemplateListResponseBodySmsTemplateListReason `json:"reason,omitempty" xml:"Reason,omitempty" type:"Struct"`
	TemplateCode    *string                                                         `json:"templateCode,omitempty" xml:"TemplateCode,omitempty"`
	TemplateContent *string                                                         `json:"templateContent,omitempty" xml:"TemplateContent,omitempty"`
	TemplateName    *string                                                         `json:"templateName,omitempty" xml:"TemplateName,omitempty"`
	TemplateType    *int32                                                          `json:"templateType"`
}
type TemplateSdk struct {
	Config *model.SMSConfig `json:"sms_config,omitempty"`
}

func (app *TemplateSdk) GetTemplateList(list *[]Template) error {
	client, err := AliClient(app.Config)
	if err != nil {
		return err
	}

	querySmsTemplateListRequest := &dysmsapi.QuerySmsTemplateListRequest{}
	// 复制代码运行请自行打印 API 的返回值
	res, _err := client.QuerySmsTemplateList(querySmsTemplateListRequest)
	if _err != nil {
		return _err
	}

	for _, item := range res.Body.SmsTemplateList {
		tl := Template{
			AuditStatus:     item.AuditStatus,
			CreateDate:      item.CreateDate,
			OrderId:         item.OrderId,
			Reason:          item.Reason,
			TemplateCode:    item.TemplateCode,
			TemplateContent: item.TemplateContent,
			TemplateName:    item.TemplateName,
			TemplateType:    item.TemplateType,
		}
		*list = append(*list, tl)
	}
	return nil
}

func (app *TemplateSdk) AddTemplate(addTemplate *Template) error {
	templateName := addTemplate.TemplateName
	templateContent := addTemplate.TemplateContent
	if templateName == nil || templateContent == nil || len(*templateName) == 0 || len(*templateContent) == 0 {
		return errors.New("添加模板缺少必要信息")
	}
	client, err := AliClient(app.Config)
	if err != nil {
		return err
	}
	smsType := int32(1)
	remark := "短信消息通知"
	if _, err := client.AddSmsTemplate(&dysmsapi.AddSmsTemplateRequest{
		OwnerId:              nil,
		Remark:               &remark,
		ResourceOwnerAccount: nil,
		ResourceOwnerId:      nil,
		TemplateContent:      templateContent,
		TemplateName:         templateName,
		TemplateType:         &smsType,
	}); err != nil {
		return err
	}
	return nil
}

func (app *TemplateSdk) DeleteTemplate(templateCode string) error {
	if len(templateCode) == 0 {
		return errors.New("删除模板缺少必要信息")
	}
	client, err := AliClient(app.Config)
	if err != nil {
		return err
	}
	if _, err := client.DeleteSmsTemplate(&dysmsapi.DeleteSmsTemplateRequest{
		TemplateCode: &templateCode,
	}); err != nil {
		return err
	}
	return nil
}
