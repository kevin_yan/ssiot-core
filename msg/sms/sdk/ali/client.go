package ali

import (
	"gitee.com/sansaniot/ssiot-core/msg/sms/model"
	openapi "github.com/alibabacloud-go/darabonba-openapi/client"
	dysmsapi "github.com/alibabacloud-go/dysmsapi-20170525/v2/client"
)

// 使用AK&SK初始化账号Client
func AliClient(config *model.SMSConfig) (_result *dysmsapi.Client, err error) {
	//if config == nil {
	//	err = sms.InitSMSConfig(nil)
	//}
	if err != nil {
		return nil, err
	}
	cfg := &openapi.Config{
		AccessKeyId:     &config.AccessKeyId,
		AccessKeySecret: &config.AccessKeySecret,
	}
	//_result = &dysmsapi.Client{}
	_result, err = dysmsapi.NewClient(cfg)
	return _result, err
}
