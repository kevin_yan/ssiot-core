package msgtype

type MsgType struct {
	//消息类型（see msgtype.MsgType）
	MsgCode string

	//消息名称（邮件主题）
	Name string

	//消息内容
	Msg string

	//短信模板code
	TemplateCode string
}

func GetMessage(code string) (msgType MsgType) {
	switch code {
	case RegCode.MsgCode:
		msgType = RegCode

	case RegSubmit.MsgCode:
		msgType = RegSubmit

	case ApproveOk.MsgCode:
		msgType = ApproveOk

	case ApproveReject.MsgCode:
		msgType = ApproveReject

	case RegNotifyHd.MsgCode:
		msgType = RegNotifyHd

	default:

	}
	return msgType
}

var RegCode MsgType = MsgType{
	MsgCode:      "0",
	Name:         "获取验证码",
	TemplateCode: "SMS_197395046",
	Msg: `
		<H1>【Walle平台注册验证码】</H1>
		<h4>亲爱的用户，您好！</h4>
		<div style="line-height: 30px;border:1px solid #cccccc;padding: 10px">
			<div style="font-weight: bold">
				<span>您的注册验证码：</span>
				<span style="color:red;font-size: 20px">${code}</span>
			</div>
			<div style="font-size: 13px;">
				(请输入该验证码完成注册，验证码5分钟内有效！)
			</div>
		
			<div style="margin-top: 10px;padding-top5pxborder-top: 1px solid #e8e8e8;color: #777">
				如果你未申请注册服务，请忽略该邮件。
			</div>
		</div>
		<div style="padding-top: 10px;color: #777">
			如果仍有问题，请拨打我们的服务专线: 400-00-64288
		</div>
	`,
}

var RegSubmit MsgType = MsgType{
	MsgCode:      "1",
	Name:         "用户注册通知",
	TemplateCode: "SMS_197385163",
	Msg: `
		<H1>【Walle平台通知】</H1>
		
		<h4>亲爱的用户，您好！</h4>
		<div style="line-height: 30px;border:1px solid #cccccc;padding: 10px">
			感谢您申请试用Walle平台，我们将在<span style="color: red;font-weight: bold">三个工作日</span>内审核您的使用申请；审核结果会以短信或邮件的形式通知您，请注意查收。
		</div>
		</div>
		<div style="padding-top: 10px;color: #777">
			如果仍有问题，请拨打我们的服务专线: 400-00-64288
		</div>
	`,
}

var ApproveOk MsgType = MsgType{
	MsgCode:      "2",
	Name:         "注册审核成功通知",
	TemplateCode: "SMS_196659968",
	Msg: `
		<H1>【Walle平台通知】</H1>
		
		<h4>亲爱的用户，您好！</h4>
		<div style="line-height: 30px;border:1px solid #cccccc;padding: 10px">
			感谢您申请试用Walle，您的登录账号为<span style="color: red;font-weight: bold">${username}</span>，登录密码为申请时设置的密码，请妥善保管。
		</div>
		</div>
		<div style="padding-top: 10px;color: #777">
			如果仍有问题，请拨打我们的服务专线: 400-00-64288
		</div>
	`,
}

var ApproveReject MsgType = MsgType{
	MsgCode:      "3",
	Name:         "注册审核拒绝通知",
	TemplateCode: "SMS_197385175",
	Msg: `
		<!-- 审批不通过 -->
		<H1>【Walle平台通知】</H1>
		
		<h4>亲爱的用户，您好！</h4>
		<div style="line-height: 30px;border:1px solid #cccccc;padding: 10px">
			感谢您申请试用Walle平台，很抱歉，您暂时不符合平台的试用规则，本次审核结果为<span style="color: red;font-weight: bold">不通过</span>
		</div>
		</div>
		<div style="padding-top: 10px;color: #777">
			如果仍有问题，请拨打我们的服务专线: 400-00-64288
		</div>
	`,
}

var RegNotifyHd MsgType = MsgType{
	MsgCode:      "4",
	Name:         "注册通知",
	TemplateCode: "SMS_259675687",
	Msg: `
		<!-- 用户注册 -->
		<H1>【Walle平台通知】</H1>
		
		<h4>亲爱的用户，您好！</h4>
		<div style="line-height: 30px;border:1px solid #cccccc;padding: 10px">
			WALLE云有新用户注册啦！注册公司：<span style="color: red;font-weight: bold">${company}</span>，<span style="color: red;font-weight: bold">联系人${contacts}</span>，<span style="color: red;font-weight: bold">手机号：${phone}</span>，请登录平台查看！
		</div>
		</div>
		<div style="padding-top: 10px;color: #777">
			如果仍有问题，请拨打我们的服务专线: 400-00-64288
		</div>
	`,
}
var TestMsg MsgType = MsgType{
	MsgCode:      "10",
	Name:         "测试通知",
	TemplateCode: "SMS_259675687",
	Msg: `
		<!-- 测试邮件 -->
		<H1>【Walle平台测试邮件】</H1>
		<h4>该邮件仅用于测试。</h4>
		<div style="line-height: 30px;border:1px solid #cccccc;padding: 10px">
			<div style="color:red;font-weight: bold">
				<span>系统自动发送,请勿回复本邮件！</span>
			</div>
			<div style="font-size: 13px;">
				发送时间：${dataTime}
			</div>
		</div>
		<div style="padding-top: 10px;color: #777">
			如果仍有问题，请拨打我们的服务专线: 400-00-64288
		</div>
		`,
}

var Cust MsgType = MsgType{}
