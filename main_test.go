package main

import (
	"fmt"
	"gitee.com/sansaniot/ssiot-core/facade/env"
	"gitee.com/sansaniot/ssiot-core/msg/sms"
	"gitee.com/sansaniot/ssiot-core/msg/sms/model"
	"strings"
	"testing"
	"time"
)

func TestConfigOnly(t *testing.T) {
	// 初始化配置
	env.SetupConfig("./settings.template.yml", true)

	// 使用日志，打印默认配置节点和自定义节点
	log := env.Log
	log.Infof("通过默认配置对象获取数据库连接：%s", env.DefaultConfig.Database.Source)
	log.Infof("通过配置操作对象获取数据库连接：%s", env.ConfigOperator.Get("ssiot.database.source"))

	log.Infof("通过默认配置对象获取mqtt用户名：%s", env.DefaultConfig.Mqtt.User)
	log.Infof("通过默认配置对象获取mqtt密码：%s", env.DefaultConfig.Mqtt.Passwd)
}

func TestSetupAll(t *testing.T) {
	env.SetupConfig("./settings.template.yml", false)

	// 使用日志，打印默认配置节点和自定义节点
	log := env.Log
	log.Infof("通过默认配置对象获取数据库连接：%s", env.DefaultConfig.Database.Source)
	log.Infof("通过配置操作对象获取数据库连接：%s", env.ConfigOperator.Get("ssiot.database.source"))

	// 设置缓存
	_ = env.Cache.Set("token", "mytoken", 100)
	token, _ := env.Cache.Get("token")
	log.Infof("获取缓存 key：%v", token)
	h1 := log.WithFields(map[string]interface{}{"key": "val"})
	h1.Trace("trace_msg")
	h1.Warn("warn_msg")

	// increase
	_, _ = env.Cache.HIncrBy("incr", "count", 1)
	_ = env.Cache.Expire("incr", 100*time.Second)
}

func TestSMS(t *testing.T) {
	conf := model.SMSConfig{
		AccessKeyId:     "600220",
		AccessKeySecret: "",
		SignName:        "1069600220",
		Vendor:          model.Vendor_JUMENG,
	}
	_ = sms.InitSMSConfig(&conf)

	//发送验证码
	//err := sms.SendSMS(model.SMSParam{
	//	Phone: "15527557427",
	//	Data:  "1234",
	//})

	//发送告警
	param := make(map[string]string)
	param["devName"] = "设备发电房撒范德萨发"
	param["variableName"] = "变量发电房撒范德萨发"
	param["alarmType"] = "环境温度超限"
	param["alarmLevel"] = "二级告警"
	param["currentVal"] = "545.6"
	err := sms.SendSMSByData(model.SMSCustParam{
		Phone: strings.Split("15527557427", ","),
		Param: param,
		Code:  "",
	})
	fmt.Println(err)

	//查询发送状态
	//sdk := jumeng.Sdk{
	//	Config: &conf
	//}
	//resp, err := sdk.GetSendStatus("2310111001591397732", "15527557427")
	//fmt.Println(resp, err)
}
