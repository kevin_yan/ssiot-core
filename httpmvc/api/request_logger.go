package api

import (
	"gitee.com/sansaniot/ssiot-core/facade/runtime"
	"gitee.com/sansaniot/ssiot-core/httpmvc"
	"gitee.com/sansaniot/ssiot-core/logger"
	"github.com/gin-gonic/gin"
	"strings"
)

type loggerKey struct{}

// GetRequestLogger 获取上下文提供的日志
func GetRequestLogger(c *gin.Context) *logger.Helper {
	var log *logger.Helper
	l, ok := c.Get(httpmvc.LoggerKey)
	if ok {
		ok = false
		log, ok = l.(*logger.Helper)
		if ok {
			return log
		}
	}
	//如果没有在上下文中放入logger
	requestId := httpmvc.GenerateMsgIDFromContext(c)
	log = logger.NewHelper(runtime.Runtime.GetLogger()).WithFields(map[string]interface{}{
		strings.ToLower(httpmvc.TrafficKey): requestId,
	})
	return log
}

// SetRequestLogger 设置logger中间件
func SetRequestLogger(c *gin.Context) {
	requestId := httpmvc.GenerateMsgIDFromContext(c)
	log := logger.NewHelper(runtime.Runtime.GetLogger()).WithFields(map[string]interface{}{
		strings.ToLower(httpmvc.TrafficKey): requestId,
	})
	c.Set(httpmvc.LoggerKey, log)
}
