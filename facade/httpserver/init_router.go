package httpserver

import (
	jwt "gitee.com/sansaniot/ssiot-core/httpmvc/jwtauth"
	"github.com/gin-gonic/gin"
)

// runAuthRouter 无需认证的路由示例
func runRouter(r *gin.Engine, routers []func(*gin.RouterGroup)) {
	// 可根据业务需求来设置接口版本
	g := r.Group("")

	for _, f := range routers {
		f(g)
	}
}

// runRouter 需要认证的路由示例
func runAuthRouter(r *gin.Engine, authMiddleware *jwt.GinJWTMiddleware, authRouters []func(v1 *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware)) {
	// 可根据业务需求来设置接口版本
	g := r.Group("")

	for _, f := range authRouters {
		f(g, authMiddleware)
	}
}
