package env

import (
	"gitee.com/sansaniot/ssiot-core/config"
	"gitee.com/sansaniot/ssiot-core/config/tool/source/file"
	"gitee.com/sansaniot/ssiot-core/facade/runtime"
	"gitee.com/sansaniot/ssiot-core/logger"
	"gitee.com/sansaniot/ssiot-core/storage"
	"gitee.com/sansaniot/ssiot-core/storage/database"
	"github.com/spf13/viper"
	"gorm.io/gorm"
	"math/rand"
	"time"
)

var Db *gorm.DB
var Log *logger.Helper
var Cache storage.AdapterCache
var ConfigOperator *viper.Viper
var DefaultConfig *config.Config
var configFile string
var extConfig interface{}

func RegisterExtConfig(config interface{}) {
	extConfig = config
}

// SetupConfig 配置初始化
func SetupConfig(configFilePath string, configOnly bool) {
	configFile = configFilePath

	// 全局调用一次 随机数种子
	rand.Seed(time.Now().UnixNano())

	// 注入配置扩展项
	config.ExtendConfig = &extConfig

	// 读取默认配置
	config.Setup(
		file.NewSource(file.WithPath(configFile)),
	)

	DefaultConfig = &config.Config{
		Http:      config.HttpConfig,
		Mqtt:      config.MqttConfig,
		Ssl:       config.SslConfig,
		Logger:    config.LoggerConfig,
		Jwt:       config.JwtConfig,
		Database:  config.DatabaseConfig,
		Databases: &config.DatabasesConfig,
		Gen:       config.GenConfig,
		Cache:     config.CacheConfig,
		Queue:     config.QueueConfig,
		Locker:    config.LockerConfig,
		Extend:    config.ExtendConfig,
	}

	// 只初始化配置和日志
	Log = logger.NewHelper(runtime.Runtime.GetLogger())

	// 初始化通过节点路径读取配置的对象
	setupConfigOperator()

	if configOnly == false {
		// 初始化所有配置
		setupAll()
	}
}

// Setup 配置Cache组件
func setupCache() {
	//4. 设置缓存
	cacheAdapter, err := config.CacheConfig.Setup()
	if err != nil {
		logger.Fatalf("cache setup error, %s\n", err.Error())
	}
	runtime.Runtime.SetCacheAdapter(cacheAdapter)
	//5. 设置验证码store
	//captcha.SetStore(captcha.NewCacheStore(cacheAdapter, 600))

	//6. 设置队列
	if !config.QueueConfig.Empty() {
		if q := runtime.Runtime.GetQueueAdapter(); q != nil {
			q.Shutdown()
		}
		queueAdapter, err := config.QueueConfig.Setup()
		if err != nil {
			logger.Fatalf("queue setup error, %s\n", err.Error())
		}
		runtime.Runtime.SetQueueAdapter(queueAdapter)
		defer func() {
			go queueAdapter.Run()
		}()
	}

	//7. 设置分布式锁
	if !config.LockerConfig.Empty() {
		lockerAdapter, err := config.LockerConfig.Setup()
		if err != nil {
			logger.Fatalf("locker setup error, %s\n", err.Error())
		}
		runtime.Runtime.SetLockerAdapter(lockerAdapter)
	}
}

// Setup 配置数据库
func setupDataBase() {
	for k := range config.DatabasesConfig {
		database.SetupDatabase(k, config.DatabasesConfig[k])
	}
}

func setupAll() {
	// 读取固定配置，及数据库、缓存、日志、队列初始化
	config.Setup(
		file.NewSource(file.WithPath(configFile)),
		setupDataBase,
		setupCache,
	)

	// Db 单个数据库时，key是*，使用任何key都可以读取
	Db = runtime.Runtime.GetDbByKey("db")
	Cache = runtime.Runtime.GetCacheAdapter()
}

// 初始化通过节点路径读取配置的对象
func setupConfigOperator() {
	//初始化配置文件参数
	viperConfig := viper.New()
	logger.Infof("config path: %s", configFile)
	//配置文件路径
	viperConfig.SetConfigFile(configFile)
	//尝试进行配置读取
	if err := viperConfig.ReadInConfig(); err != nil {
		// 配置文件未找到错误；如果需要可以忽略
		println("config path err=[%v]", err)
	}
	ConfigOperator = viperConfig
}
