package file

import (
	"gitee.com/sansaniot/ssiot-core/config/tool/encoder"
	"strings"
)

func format(p string, e encoder.Encoder) string {
	parts := strings.Split(p, ".")
	if len(parts) > 1 {
		return parts[len(parts)-1]
	}
	return e.String()
}
