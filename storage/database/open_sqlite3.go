//go:build sqlite3
// +build sqlite3

package database

import (
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var opens = map[string]func(string) gorm.Dialector{
	"postgres": postgres.Open,
	"sqlite3":  sqlite.Open,
}
