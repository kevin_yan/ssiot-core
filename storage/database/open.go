//go:build !sqlite3
// +build !sqlite3

package database

import (
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

var opens = map[string]func(string) gorm.Dialector{
	"postgres": postgres.Open,
	"sqlserver": func(dsn string) gorm.Dialector {
		return sqlserver.Open(dsn)
	},
	"mysql": mysql.Open,
	"sqlite": func(dsn string) gorm.Dialector {
		return sqlite.Open(dsn)
	},
}
