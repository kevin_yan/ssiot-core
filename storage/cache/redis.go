package cache

import (
	"fmt"
	"time"

	"github.com/go-redis/redis/v7"
)

// NewRedis redis模式
func NewRedis(client *redis.Client, options *redis.Options) (*Redis, error) {
	if client == nil {
		client = redis.NewClient(options)
	}
	r := &Redis{
		client: client,
	}
	err := r.connect()
	if err != nil {
		return nil, err
	}
	return r, nil
}

// Redis cache implement
type Redis struct {
	client *redis.Client
}

func (*Redis) String() string {
	return "redis"
}

// connect connect test
func (r *Redis) connect() error {
	var err error
	_, err = r.client.Ping().Result()
	return err
}

// Get from key
func (r *Redis) Get(key string) (string, error) {
	return r.client.Get(key).Result()
}

// Set value with key and expire time
func (r *Redis) Set(key string, val interface{}, expire int) error {
	return r.client.Set(key, val, time.Duration(expire)*time.Second).Err()
}

// HExists hash表中是否存在key
func (r *Redis) HExists(hk, key string) (bool, error) {
	return r.client.HExists(hk, key).Result()
}

func (r *Redis) HashSet(key string, val ...interface{}) error {
	return r.client.HSet(key, val...).Err()
}

// Del delete key in redis
func (r *Redis) Del(key string) error {
	return r.client.Del(key).Err()
}

// HashGet from key
func (r *Redis) HashGet(hk, key string) (string, error) {
	return r.client.HGet(hk, key).Result()
}

// HashGetAll from key
func (r *Redis) HashGetAll(hk string) (map[string]string, error) {
	return r.client.HGetAll(hk).Result()
}

// HashDel delete key in specify redis's hashtable
func (r *Redis) HashDel(hk, key string) error {
	return r.client.HDel(hk, key).Err()
}

// Increase
func (r *Redis) Increase(key string) error {
	return r.client.Incr(key).Err()
}

func (r *Redis) Decrease(key string) error {
	return r.client.Decr(key).Err()
}

// Set ttl
func (r *Redis) Expire(key string, dur time.Duration) error {
	return r.client.Expire(key, dur).Err()
}

// GetClient 暴露原生client
func (r *Redis) GetClient() *redis.Client {
	return r.client
}

func (r *Redis) HIncrBy(key, field string, increment int) (int64, error) {
	return r.client.HIncrBy(key, field, int64(increment)).Result()
}

func (r *Redis) ScanKey(key string) []string {
	//遍历的起时位置
	var cursor uint64 = 0
	//缓存所有的key集合
	var keyList []string
	//每次遍历的最大值
	var maxPage int64 = 500
	//循环查询结果
	for {
		//缓存错误的信息
		var err error
		var keys []string
		//*扫描所有key，每次查询maxPage条 分页查询
		keys, cursor, err = r.client.Scan(cursor, key, maxPage).Result()
		//本次遍历错误
		if err != nil {
			//错误
			_ = fmt.Errorf("redis模糊查询异常 err=[%+v]", err)
			//存在错误 跳过
			break
		}
		//缓存遍历的结果
		keyList = append(keyList, keys...)
		//遍历完成 跳出
		if cursor == 0 {
			break
		}
	}
	//返回结果
	return keyList
}

// 列表头部添加
func (r *Redis) LPush(key string, values ...interface{}) (int64, error) {
	return r.client.LPush(key, values...).Result()
}

// 列表尾部添加
func (r *Redis) RPush(key string, values ...interface{}) error {
	_, err := r.client.RPush(key, values...).Result()
	return err
}

// 列表尾部读取(阻塞)
func (r *Redis) BRPop(timeout time.Duration, keys ...string) ([]string, error) {
	return r.client.BRPop(timeout, keys...).Result()
}

// 列表尾部读取(非阻塞)
func (r *Redis) RPop(key string) (string, error) {
	return r.client.RPop(key).Result()
}

func (r *Redis) LRange(key string, start, stop int64) ([]string, error) {
	return r.client.LRange(key, start, stop).Result()
}
