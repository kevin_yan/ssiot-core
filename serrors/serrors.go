package serrors

import (
	"fmt"
)

// 错误代码
const (
	OK                = 200
	Fail              = 201
	InvalidParam      = 202
	WrongUser         = 203
	WrongPassword     = 204
	WrongPhoneNumber  = 205
	InsertFail        = 206
	UpdateFail        = 207
	DeleteFail        = 208
	ModifyFail        = 209
	UploadFail        = 210
	SelectFail        = 211
	ProcessCreateFail = 300
	ProcessStartFail  = 301
	ProcessDuplicate  = 302
	ProcessUnexist    = 303
	ProcessDelFail    = 304
	ProcessFileLack   = 305
)

// 错误消息

var code2msg = map[int]string{
	OK:                "操作成功",
	Fail:              "操作失败",
	InvalidParam:      "请求参数错误",
	WrongUser:         "用户名不正确",
	WrongPassword:     "密码不正确",
	InsertFail:        "添加失败",
	UpdateFail:        "更新失败",
	DeleteFail:        "删除失败",
	ModifyFail:        "修改失败",
	SelectFail:        "查找失败",
	UploadFail:        "上传失败",
	WrongPhoneNumber:  "不被识别的手机号",
	ProcessCreateFail: "创建进程失败",
	ProcessStartFail:  "进程启动失败",
	ProcessDuplicate:  "进程重复",
	ProcessUnexist:    "进程不存在",
	ProcessDelFail:    "进程删除失败",
	ProcessFileLack:   "缺少进程文件或目录",
}

func Msg(code int) string {
	return code2msg[code]
}

// SError 三三物联网Error定义
type SError struct {
	code int
	msg  string
}

func NewSError(code int) *SError {
	return &SError{
		code,
		code2msg[code],
	}
}

func (e *SError) Error() string {
	return fmt.Sprintf("Error: %+v %+v ", e.code, e.msg)
}

func (e *SError) Code() int {
	return e.code
}

func (e *SError) Msg() string {
	return e.msg
}

func (e *SError) Equal(errorCode int) bool {
	if e.Code() == errorCode {
		return true
	}

	return false
}
